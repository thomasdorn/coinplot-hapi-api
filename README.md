## Compiled HAPI project example with nexe

The project will transpile your hapi application to a single file and then compile it with nexe

### Steps to build and run
######Use nvm to install node 8.15.0 https://github.com/creationix/nvm
```sh
$ git clone https://gitlab.com/thomasdorn/coinplot-hapi-api.git
$ cd coinplot-hapi-api
$ npm install
$ gulp compile && ./bundle
```


### To view server output
```sh
http://localhost:8000/code
```
