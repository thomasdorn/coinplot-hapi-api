'use strict';

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

var _hapi = require('hapi');

var Hapi = _interopRequireWildcard(_hapi);

var _couponCode = require('coupon-code');

var cc = _interopRequireWildcard(_couponCode);

var server = Hapi.server({
  host: 'localhost',
  port: 8000
});

// Add the route
server.route({
  method: 'GET',
  path: '/hello',
  handler: function handler(request, h) {

    return 'hello world';
  }
});
server.route({
  method: 'GET',
  path: '/code',
  handler: function handler(request, h) {

    return cc.generate();
  }
});
// Start the server
var start = function start() {
  return regeneratorRuntime.async(function start$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.prev = 0;
        context$1$0.next = 3;
        return regeneratorRuntime.awrap(server.start());

      case 3:
        context$1$0.next = 9;
        break;

      case 5:
        context$1$0.prev = 5;
        context$1$0.t0 = context$1$0['catch'](0);

        console.log(context$1$0.t0);
        process.exit(1);

      case 9:

        console.log('Server running at:', server.info.uri);

      case 10:
      case 'end':
        return context$1$0.stop();
    }
  }, null, this, [[0, 5]]);
};

start();